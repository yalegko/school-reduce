package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	_ "github.com/chrislusf/glow/driver"
	"github.com/chrislusf/glow/flow"

	"school-reduce/answer"
)

type teamTries struct {
	Name   string
	NTries int
}

var (
	topFlow  *flow.Dataset
	nShards  int
	dumpPath string
	logDebug bool
)

// Create flow on initialize to allow us to preprocess it
func initFlow() {
	answers := flow.New().TextFile(
		dumpPath, nShards,
	).Filter(func(line string) bool {
		// Skip comments.
		return !strings.HasPrefix(line, "#")
	}).Map(func(line string) []string {
		// Split string into rows.
		return strings.Split(line, "∫")
	}).Map(func(tokens []string, ch chan *answer.Answer) {
		// Parse into Answer objects.
		ans, err := answer.NewAnswer(tokens)
		if err != nil {
			fmt.Printf("Can't parse line %+v; Error %v", tokens, err)
		}
		ch <- ans
	})

	topFlow = answers.Map(func(ans *answer.Answer) flow.KeyValue {
		// Form it into (key; value) pairs.
		return flow.KeyValue{
			Key:   ans.Team.Name,
			Value: 1,
		}
	}).ReduceByKey(func(x int, y int) int {
		// Reduce all pairs with the same key.
		return x + y
	}).Map(func(teamName string, count int) flow.KeyValue {
		// Swap pairs to (count; teamName) cos Sort will work with keys.
		return flow.KeyValue{
			Key:   count,
			Value: teamName,
		}
	}).Sort(func(aCount int, bCount int) bool {
		// Sort function if "less than". We return >= to reverse order.
		return aCount >= bCount
	}).Map(func(count int, teamName string) teamTries {
		// Place them in struct for convinience.
		return teamTries{
			Name:   teamName,
			NTries: count,
		}
	})
}

func main() {
	///////////////////////////////////////////////////////////////////////////
	// Flag definitions:
	// dumpPath flag allow to specify dump file location.
	flag.StringVar(&dumpPath, "dumpPath", "answers-dump.txt",
		"Path to the dump file")
	// nShards flag can be used to split work into different number of
	// separate execution flows so you can play with it.
	flag.IntVar(&nShards, "nShards", 3, "Number of shards to split the work")
	// logDebug flag can be used to enable additional logging to stderr.
	flag.BoolVar(&logDebug, "logDebug", false, "Provide glow logs to stderr")
	///////////////////////////////////////////////////////////////////////////

	// Parse all that magic glow flags and do init work
	flag.Parse()

	// Honestly we should create flow statically in the "init()" function, but
	// for testing purposes we use dynamic parameters so create the flow in
	// runtime.
	initFlow()
	flow.Ready()

	// Set log level (no logs is default case)
	setLogging()

	var (
		out        = make(chan teamTries)
		nDisplayed int
	)
	// "go" is important! Ohterwise we will block it forever, cos mapreducer
	// will wait somebody to read from channel.
	go topFlow.AddOutput(out).Run()

	// Some fancy result printing.
	fmt.Printf("%20s | %15s\n", "Team name", "Number of tries")
	fmt.Println(strings.Repeat("-", 20+3+15))
	for t := range out {
		fmt.Printf("%20s | %15d\n", t.Name, t.NTries)
		nDisplayed++
		if nDisplayed == 10 {
			break
		}
	}

	// Closing everything and garbage collecting is for girls - just exit now.
}

func setLogging() {
	if logDebug {
		log.SetOutput(os.Stderr)
	} else {
		// Discard logs to beautify output.
		log.SetOutput(ioutil.Discard)
	}
}
