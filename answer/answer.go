package answer

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"
)

const (
	cNParts     = 7
	cTimeLayout = "2006-01-02 15:04:05"
)

// Team represents team structure.
type Team struct {
	Name        string
	IsSchool    bool
	CountryCode string
}

// Task represents task structure.
type Task struct {
	Title  string
	FlagRE *regexp.Regexp
}

// Answer represents answer structure.
type Answer struct {
	Team     Team
	Task     Task
	Answer   string
	TimeSent time.Time
}

// NewAnswer returns Answer struct from list of parts.
func NewAnswer(parts []string) (*Answer, error) {
	if len(parts) != cNParts {
		return nil, fmt.Errorf(
			"Not enough parts to parse answer. Got %d, expected %d.",
			len(parts), cNParts,
		)
	}
	re, err := regexp.Compile(parts[6])
	if err != nil {
		return nil, fmt.Errorf(
			"Can't compile regular expression for flag, given: '%s'.",
			parts[6],
		)
	}
	timeParts := strings.Split(parts[5], ".")
	if len(timeParts) < 1 {
		return nil, errors.New("Can't split time by '.' before parsing.")
	}
	timestamp, err := time.Parse(cTimeLayout, timeParts[0])
	if err != nil {
		return nil, fmt.Errorf(
			"Can't parse timestamp '%v'.",
			parts[5],
		)
	}
	ans := Answer{
		Team: Team{
			Name:        parts[0],
			IsSchool:    isSchool(parts[1]),
			CountryCode: parts[2],
		},
		Task: Task{
			Title:  parts[3],
			FlagRE: re,
		},
		Answer:   parts[4],
		TimeSent: timestamp,
	}
	return &ans, nil
}

func isSchool(v string) bool {
	if v == "t" {
		return true
	}
	return false
}
