## Prepare go environment

1. Check go install:
  
    `$go version`

    You should get something like
    
    `go version go1.8 linux/amd64`

    If not - use prepared [script](etc/go_env.sh) to install go and environment.

2. Clone the repo (beware, path is important!):
  ```bash
    cd $GOPATH/src
    git clone https://gitlab.com/yalegko/school-reduce.git
  ```

3. Install requirements
  ```bash
    cd $GOPATH/src/school-reduce
    go get -v ./...
  ```

4. You are ready to go!

## Run locally

Example for local usage is placed at [example-local/main.go](./example-local/main.go)
To run it just use:
```bash
  cd $GOPATH/src/school-reduce
  go run example-local/main.go
```

Or you can play seprating execution flow into different number of parts:
```bash
  go build -o local_reduce example-local/main.go
  time ./local_reduce -nShards=5
```

## Run in the local cluster

Example for usage in cluster is placed at [example-cluster/main.go](./example-cluster/main.go)

To run it we need to setup cluster:
1. Install `glow` tool
  ```bash
    go install github.com/chrislusf/glow
  ```

2. Check the installation typing for e.g. just `glow`. 
Your should see help message. If not - go to **Prepare go environment** section.

3. Setup the local cluster using provided script:
  ```bash
    cd $GOPATH/src/school-reduce  
    bash ./etc/setup_local_cluster.sh
  ```

4. Run your code in cluster mode:
  ```bash
    go run example-cluster/main.go -glow
  ```

## Run in the prepared cluster

To run the code in prepared cluster ask the admin about
- `answers-dump.txt` file path (`FILE_PATH`)
- cluster leader addres (`CLUSTER_ADDR`)

Then just provide necessary flags and you are ready to go:
```bash
  cd $GOPATH/src/school-reduce
  go build -o cluster_reduce example-cluster/main.go
  ./cluster_reduce -dumpPath="FILE_PATH" -glow -glow.leader="CLUSTER_ADDR"
```

You can also play with `nShards` flag and `time` command:
```bash
  time ./cluster_reduce -nShards=5 -dumpPath="FILE_PATH" -glow -glow.leader="CLUSTER_ADDR"
```

## Visualize the flow

To understand how each executor works, you can visualize the flow by
generating a dot file of the flow, and render it to png file via "dot" command provided from graphviz.

```bash
  cd $GOPATH/src/school-reduce
  go build -o cluster_reduce example-cluster/main.go
  ./cluster_reduce -nShards=5 -glow -glow.flow.plot > /tmp/x.dot
  dot -Tpng -o /tmp/test.png /tmp/x.dot
```

## FAQ

Q: Can I see some error logs in cluster binary? <br>
A: Just add `-logDebug` flag running it.

Q: Can I try more examples of queries? <br>
A: You can try to implement following:
- Find the number of teams participate from each country
- Find the country with the largest number of incorrect task submits
- Find the task with the largest number of incorrect submits
- Find the task that was solved most
- Find the most popular incorrect flag
- Find the most popular incorrect flag for every country
- Find the number of incorrect submits for the top 10 winner teams
- Find the number of the correct flag submits in the last 10 minutes of the game (assume that game was over at 20:00)