package main

import (
	"flag"
	"fmt"
	"strings"

	"github.com/chrislusf/glow/flow"

	"school-reduce/answer"
)

type teamTries struct {
	Name   string
	NTries int
}

func main() {
	// nShards flag can be used to split work into different number of
	// separate execution flows so you can play with it.
	var nShards int
	flag.IntVar(&nShards, "nShards", 3, "Number of shards to split the work")
	flag.Parse()

	answers := flow.New().TextFile(
		"answers-dump.txt", nShards,
	).Filter(func(line string) bool {
		// Skip comments.
		return !strings.HasPrefix(line, "#")
	}).Map(func(line string) []string {
		// Split string into rows.
		return strings.Split(line, "∫")
	}).Map(func(tokens []string, ch chan *answer.Answer) {
		// Parse into Answer objects.
		ans, err := answer.NewAnswer(tokens)
		if err != nil {
			fmt.Printf("Can't parse line %+v; Error %v", tokens, err)
		}
		ch <- ans
	})

	top := answers.Map(func(ans *answer.Answer) flow.KeyValue {
		// Form it into (key; value) pairs.
		return flow.KeyValue{
			Key:   ans.Team.Name,
			Value: 1,
		}
	}).ReduceByKey(func(x int, y int) int {
		// Reduce all pairs with the same key.
		return x + y
	}).Map(func(teamName string, count int) flow.KeyValue {
		// Swap pairs to (count; teamName) cos Sort will work with keys.
		return flow.KeyValue{
			Key:   count,
			Value: teamName,
		}
	}).Sort(func(aCount int, bCount int) bool {
		// Sort function if "less than". We return >= to reverse order.
		return aCount >= bCount
	}).Map(func(count int, teamName string) teamTries {
		// Place them in struct for convinience.
		return teamTries{
			Name:   teamName,
			NTries: count,
		}
	})

	var (
		out        = make(chan teamTries)
		nDisplayed int
	)
	// "go" is important! Ohterwise we will block it forever, cos mapreducer
	// will wait somebody to read from channel.
	go top.AddOutput(out).Run()

	// Some fancy result printing.
	fmt.Printf("%20s | %15s\n", "Team name", "Number of tries")
	fmt.Println(strings.Repeat("-", 20+3+15))
	for t := range out {
		fmt.Printf("%20s | %15d\n", t.Name, t.NTries)
		nDisplayed++
		if nDisplayed == 10 {
			break
		}
	}

	// Closing everything and garbage collecting is for girls - just exit now.
}
