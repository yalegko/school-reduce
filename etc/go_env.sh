#!/bin/sh

# Place it to the /etc/profile.d/ or append to your .bash.rc for autouse

# Install go
if !(test -d /usr/local/go && test -x /usr/local/go); then 
    echo sooqa
    exit
    #wget -O /tmp/go.tgz https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz
    #tar -C /usr/local -xzf /tmp/go.tgz
fi

# Create user folders for go.
# Try to do it in localfolder, if not - use $HOME
if test -d /localfolder && test -x /localfolder; then
    GOPATHDIR="/localfolder/$(whoami)/go"
else 
    GOPATHDIR="$(HOME)/go"
fi
mkdir -p $GOPATHDIR/go/{src,bin}

# Export environment variables
if ! echo $PATH | grep -q /usr/local/go/bin ; then
  export PATH=$PATH:/usr/local/go/bin
  export GOPATH=$GOPATHDIR
  export PATH=$PATH:$(go env GOPATH)/bin
fi